# Rapport d'itération \#2

## Composition de l'équipe

|  &nbsp;                 | Itération précédente     |
| -------------           | -------------            |
| **Product Owner**       | *Félix GOUEDARD*         |
| **Scrum Master**        | *Damien BERNEAUX*        |
| **Dev**                 | *Anthony BARBIER*        |
| **Dev**                 | *Kévin COCCHI*           |
| **Dev**                 | *Loïc MARTIN*            |
| **Dev**                 | *Nicolas MONSINJON*      |


## Bilan de l'itération précédente

### Évènements
- Le sprint #2 s'est une fois de plus déroulé de manière smooth.

### Taux de complétion de l'itération  
- 6 terminés / 6 prévus = **100%**

### Liste des User Stories terminées
- En tant que créateur de la partie, je peux demander au bot à passer une question afin de fluidifier le rythme du jeu.
- En tant qu'utilisateur, je veux pouvoir marquer des points lorsque ma réponse est correcte.
- En tant qu'utilisateur qui répond à la question, je veux être en mesure de faire une légère erreur sur ma réponse si elle est proche de la réalité. afin de ne pas être entièrement pénalisé.
- En tant qu'utilisateur, et/ou créateur de parties, je veux pouvoir me renseigner sur toutes les commandes du bot avec un simple `!help`.
- En tant que personne ayant lancé la partie, je veux pouvoir choisir le nombre de questions afin de moduler la durée de la partie.
- En tant qu'utilisateur, je veux pouvoir voir mon score actuel afin de me rendre compte de ma performance actuelle.


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
- L'équipe était satisfaite du travail accompli et est confortée dans son choix de la méthodologie Scrum

### Actions prises pour la prochaine itération
- Continuer de proposer des améliorations pour notre projet

### Axes d'améliorations
- Faire d'avantage de rencontres en dehors du travail


## Prévisions de l'itération suivante

### Évènements prévus  
- La campagne BDE est imminente, et risque de perturber substantiellement l'avancement du projet.

### Titre des User Stories reportées
*Aucune*

### Titre des nouvelles User Stories
- En tant que gérant de serveur, je veux pouvoir proposer à plusieurs groupes d'utilisateurs de jouer simultanément sur des chans différents.
- En tant qu'utilisateur, je veux pouvoir avoir des indices sur la question.
- En tant qu'utilisateur, je veux pouvoir voir les affiches des films proposés dans la question.
- En tant que créateur du bot, je veux pouvoir déployer le bot sur plusieurs serveurs en même temps.
- En tant qu'utilisateur, je veux pouvoir consulter le score d'un autre joueur et de tous les joueurs.
- En tant que créateur de la partie, je veux que les joueurs qui demandent un indice soient pénalisés sur le score.


## Confiance

### Taux de confiance de l'équipe dans l'itération

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |

### Taux de confiance de l'équipe pour la réalisation du projet

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |
