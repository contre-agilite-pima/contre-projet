# Rapport d'itération \#3

## Composition de l'équipe

|  &nbsp;                 | Itération précédente     |
| -------------           | -------------            |
| **Product Owner**       | *Kévin COCCHI*           |
| **Scrum Master**        | *Nicolas MONSINJON*      |
| **Dev**                 | *Anthony BARBIER*        |
| **Dev**                 | *Damien BERNEAUX*        |
| **Dev**                 | *Félix GOUEDARD*         |
| **Dev**                 | *Loïc MARTIN*            |


## Bilan de l'itération précédente

### Évènements
- Il n'y a pas eu d'évènements majeurs qui ont perturbés le déroulement du sprint #3.

### Taux de complétion de l'itération
5 terminés / 5 prévus = **100%**

### Liste des User Stories terminées
- En tant que créateur du bot, je veux pouvoir déployer le bot sur plusieurs serveurs en même temps.
- Permettre d'afficher les affiches des films.
- En tant que gérant de serveur, je veux pouvoir proposer aux joueurs de jouer simultanément sur des chans différents.
- Affichage du score des autres joueurs.
- En tant qu'utilisateur, je veux pouvoir avoir des indices sur la question.


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
- L'équipe était satisfaite du travail accompli et est confortée dans son choix de la méthodologie Scrum.

### Actions prises pour la prochaine itération
- Continuer de proposer des améliorations pour le jeu des films.
- Proposer de nouvelles possibilités de jeu avec des musiques ou des animés.

### Axes d'améliorations
- Faire d'avantage de rencontres en dehors du travail.


## Prévisions de l'itération suivante

### Évènements prévus
- La campagne BDE est toujours en cours, et risque de perturber l'équipe pour la semaine à venir.

### Titre des User Stories reportées
*Aucune*

### Titre des nouvelles User Stories
- En tant que personne ayant lancé la partie, je dois éventuellement pouvoir moduler le nombre de choix dans la réponse afin de moduler la difficulté de la partie.
- En tant que personne ayant lancé la partie, je veux pouvoir consulter l'historique des questions (et leur réponse).
- En tant que personne ayant lancé la partie, je veux pouvoir arrêter une partie en cours.
- En tant que personne ayant lancé la partie, je veux pouvoir choisir la date minimale de sortie des films tirés.
- En tant que joueur et créateur de la partie, je souhaite disposer de nouvelles catégories en plus des films (Musiques, Animes).


## Confiance

### Taux de confiance de l'équipe dans l'itération

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |

### Taux de confiance de l'équipe pour la réalisation du projet

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |
