# Rapport d'itération \#4

## Composition de l'équipe

|  &nbsp;                 | Itération précédente     |
| -------------           | -------------            |
| **Product Owner**       | *Nicolas MONSINJON*      |
| **Scrum Master**        | *Félix GOUEDARD*         |
| **Dev**                 | *Anthony BARBIER*        |
| **Dev**                 | *Damien BERNEAUX*        |
| **Dev**                 | *Kévin COCCHI*           |
| **Dev**                 | *Loïc MARTIN*            |


## Bilan de l'itération précédente

### Évènements
- Aucun évènement n'a perturbé le bon déroulement du sprint #4.

### Taux de complétion de l'itération
5 terminés / 5 prévus = **100%**

### Liste des User Stories terminées
- En tant que personne ayant lancé la partie, je veux pouvoir arrêter une partie en cours.
- En tant que personne ayant lancé la partie, je veux pouvoir consulter l'historique des questions (et leur réponse).
- En tant que personne ayant lancé la partie, je dois être capable de choisir entre plusieurs réponses pour une question présentant de multiples possibilités d'acteurs en commun.
- En tant que personne ayant lancé la partie, je veux pouvoir choisir la date minimale de sortie des films tirés.
- En tant que joueur et créateur de la partie, je souhaite disposer de nouvelles catégories en plus des films (Musiques, Animes).


## Rétrospective de l'itération précédente

### Bilans des retours et des précédentes actions
- Nous avons amélioré notre gestion du Trello en augmentant le nombre de tâches sur chaque carte.

### Actions prises pour la prochaine itération
- Améliorer les possibilités de choix pour le créateur de la partie (difficulté, timer, …).
- Permettre aux joueurs un meilleur contrôle sur la partie (vote pour passer la question, …)


## Prévisions de l'itération suivante

### Évènements prévus
- Arrivée des partiels.
- Fin du projet de PIMA.

### Titre des User Stories reportées
*Aucune*

### Titre des nouvelles User Stories
Pour une éventuelle future itération, nous avions pensé à ce types d'*user stories* :
- En tant que créateur, je veux pouvoir choisir la difficulté des questions.
- En tant qu'utilisateur, je veux pouvoir voter pour passer la question en cours.


## Confiance

### Taux de confiance de l'équipe dans l'itération

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |

### Taux de confiance de l'équipe pour la réalisation du projet

|           | :( | :\ | :) | :D |
|-----------|----|----|----|----|
| Équipe 13 | 0  | 0  | 0  | 6  |
