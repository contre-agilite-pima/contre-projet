const readAnswer = function readAnswer(message, gameRunning) {
    // Verify that a game is running
    if (gameRunning) {
        // Verify that the input is not only '!r' but '!r <answer>'
        const userAnswerArray = message.content.toLowerCase().split(' ');
        userAnswerArray.shift();
        const userAnswerString = userAnswerArray.join(' ');
        return userAnswerString;
    }

    return false;
};

module.exports = readAnswer;
