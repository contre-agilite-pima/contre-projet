const fs = require('fs');

const displayScore = function displayScore(chan, message, player) {
    const playerID = player.id.toString();
    const authorID = message.author.id.toString();
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const displayName = message.guild.members.get(playerID).displayName;
    const playerAvatar = player.avatarURL;

    // if the player has not yet answered a question, put default values
    if (!obj.players[playerID]) {
        obj.players[playerID] = {
            initials_hint: false,
            image_hint: false,
            score: 0,
        };
    }

    // display an embed showing the score of the wanted player
    chan.send({
        embed: {
            description: `${(authorID === playerID) ? 'Your' : 'Their'} current score is : **${obj.players[playerID].score} points**.`,
            color: 0x2bade5,
            author: {
                name: `Score of ${displayName}`,
                icon_url: playerAvatar,
            },
        },
    });
};

module.exports = displayScore;
