const fs = require('fs');
const fetch = require('node-fetch');

const apiKey = '275a1f1a58da7e71e51c19b377488b74';
const options = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
    },
};

const getMovieCast = function getMovieCast(movieID) {
    // API route
    const url = `https://api.themoviedb.org/3/movie/${movieID}/credits?api_key=${apiKey}`;

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then((data) => {
            const { cast } = data;
            const actors = cast.map(x => x.name);
            const actorsCount = cast.length;

            // when there's not enough data to create a question, pick new data
            if (actorsCount === 0) {
                console.error('Problem : could not pick an adequate question ...');
                console.log('Picking new data.\n');
                return Promise.reject(new Error(`No actors for movie#${movieID}`));
            }

            return actors;
        });
};

const getRandomMovies = function getRandomMovies(actorID, numberOfPicks, yearlimit) {
    // API route
    const url = `https://api.themoviedb.org/3/person/${actorID}/movie_credits?api_key=${apiKey}`;

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then((data) => {
            const { cast } = data;

            // filter the cast according to the year limit in the game file (default is > 0)
            const castFiltered = cast.filter((movie) => {
                if (yearlimit.symbol === '>') {
                    return parseInt(movie.release_date, 10) > yearlimit.year;
                } else if (yearlimit.symbol === '<') {
                    return parseInt(movie.release_date, 10) < yearlimit.year;
                }
                return parseInt(movie.release_date, 10) === yearlimit.year;
            });

            // when there's not enough data to create a question, pick new data
            if (castFiltered.length < numberOfPicks) {
                console.error('Problem : could not pick an adequate question ...');
                console.log('Picking new data.\n');
                return Promise.reject(new Error(`Not enough movies for actor#${actorID}`));
            }

            // pick random movies for this actor
            return castFiltered.sort(() => 0.5 - Math.random()).slice(0, numberOfPicks);
        });
};

const requestTheMovieDB = function requestTheMovieDB(chan, numberOfPicks = 2) {
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;

    // API route
    const url = `https://api.themoviedb.org/3/person/popular?api_key=${apiKey}&page=${1 + Math.floor(Math.random() * 250)}`;

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        // pick a random actor ID
        .then((data) => {
            const pickedActor = data.results.sort(() => 0.5 - Math.random())[0];
            console.log(`Selected actor for request : ${pickedActor.name} (#${pickedActor.id})`);
            return pickedActor.id;
        })
        // pick random movies for this actor
        .then(actorID => getRandomMovies(actorID, numberOfPicks, obj.yearlimit))
        // then, add the appropriate informations from movies to the game's object
        .then((listOfMovies) => {
            listOfMovies.forEach((movie, index) => {
                obj.question[currentQuestion][index] = movie.title;
                obj.images[currentQuestion][index] = (movie.poster_path) ? `https://image.tmdb.org/t/p/w500${movie.poster_path}` : null;
                console.log(` > Question element #${index + 1} : ${obj.question[currentQuestion][index]} (${movie.id})`);
            });

            fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj)); // save the game's object
            return listOfMovies.map(x => x.id);
        })
        // retrieve the cast of each movie that was randomly picked
        .then(movieIDs => Promise.all(movieIDs.map(movieID => getMovieCast(movieID))))
        // finally, create the answer by intersecting casts
        .then((listOfCasts) => {
            listOfCasts.forEach((actors) => {
                obj.answers[currentQuestion] = actors.filter(n =>
                    (obj.answers[currentQuestion].length === 0)
                    || (obj.answers[currentQuestion].includes(n)));
            });

            fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj)); // save the game's object
            return obj.answers[currentQuestion];
        });
};

module.exports = requestTheMovieDB;
