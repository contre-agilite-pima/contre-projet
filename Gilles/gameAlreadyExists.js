const fileAlreadyExists = require(`${__dirname}/fileAlreadyExists.js`);

const gameAlreadyExists = function gameAlreadyExists(chan) {
    const chanID = chan.id.toString();
    return fileAlreadyExists(`${__dirname}/games/${chanID}.json`);
};

module.exports = gameAlreadyExists;
