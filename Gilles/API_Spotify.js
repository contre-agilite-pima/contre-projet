const fs = require('fs');
const fetch = require('node-fetch');

const clientID = '6eebcb95c1394288a51fffa927266268';
const clientSecret = '4fbc18b97134435f8211fcc9162c9c4d';

const getAccessToken = function getAccessToken() {
    // API route and options
    const url = 'https://accounts.spotify.com/api/token';
    const loginOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Basic ${Buffer.from(`${clientID}:${clientSecret}`).toString('base64')}`,
        },
        body: 'grant_type=client_credentials',
    };

    return fetch(url, loginOptions)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then(data => data.access_token);
};

const getRandomAlbums = function getRandomAlbums(artistID, options, numberOfPicks) {
    // API route
    const url = `https://api.spotify.com/v1/artists/${artistID}/albums?limit=${numberOfPicks}`;

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then((data) => {
            const albums = data.items;

            // TODO : limiter la date ici ? ==> pas de date à priori dans le json data...

            if (albums.length < numberOfPicks) {
                console.error('Problem : could not pick an adequate question ...');
                console.log('Picking new data.\n');
                return Promise.reject(new Error(`Not enough albums for artist#${artistID}`));
            }

            return albums;
        });
};

const getNumberOfArtists = function getNumberOfArtists(options) {
    // API route
    const randomYear = 1990 + Math.floor(Math.random() * 30);
    const url = `https://api.spotify.com/v1/search?q=year:${randomYear}&type=artist&market=FR`;

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then(data => ({
            artistsCount: data.artists.total,
            year: randomYear,
        }));
};

const requestSpotify = function requestSpotify(chan, numberOfPicks = 2) {
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;
    let options;
    let pickedArtist;

    return getAccessToken() // retrieve an authentication token from the API
        // update options for future queries so that we store that token
        .then((token) => {
            options = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            };
            return getNumberOfArtists(options);
        })
        // pick a random artist given a random year and a random offset on a search query
        .then((numberOfArtistsWithYear) => {
            const { artistsCount, year } = numberOfArtistsWithYear;
            const pickedOffset = 1 + Math.floor(Math.random() * artistsCount);
            const url = `https://api.spotify.com/v1/search?q=year:${year}&type=artist&market=FR&limit=1&offset=${pickedOffset}`;

            return fetch(url, options)
                .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
                // pick random albums for the artist
                .then((data) => {
                    [pickedArtist] = data.artists.items;
                    console.log(`Selected artist for request : ${pickedArtist.name} (#${pickedArtist.id})`);
                    return getRandomAlbums(pickedArtist.id, options, numberOfPicks);
                })
                // finally, add the appropriate informations from albums to the game's object and create the answer by intersecting artists
                .then((listOfAlbums) => {
                    listOfAlbums.forEach((album, index) => {
                        obj.question[currentQuestion][index] = album.name;
                        obj.images[currentQuestion][index] = (album.images.length > 0) ? album.images[0].url : null;
                        console.log(` > Question element #${index + 1} : ${obj.question[currentQuestion][index]} (#${album.id})`);
                    });

                    listOfAlbums.forEach((album) => {
                        obj.answers[currentQuestion] = album.artists.map(x => x.name).filter(n =>
                            ((obj.answers[currentQuestion].length === 0)
                            || (obj.answers[currentQuestion].includes(n)))
                            && n.toLowerCase() !== 'various artists');
                    });

                    if (!(obj.answers[currentQuestion].includes(pickedArtist.name))) {
                        obj.answers[currentQuestion].push(pickedArtist.name);
                    }

                    fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj)); // save the game's object
                    return obj.answers[currentQuestion];
                });
        });
};

module.exports = requestSpotify;
