const fs = require('fs');

const displayQuestion = require(`${__dirname}/displayQuestion.js`);
const displayAllScores = require(`${__dirname}/displayAllScores.js`);
const requestTheMovieDB = require(`${__dirname}/API_TheMovieDB.js`);
const requestAniList = require(`${__dirname}/API_AniList.js`);
const requestSpotify = require(`${__dirname}/API_Spotify.js`);

const categoryAPIs = {
    movie: { api: requestTheMovieDB, threshold: 0 },
    anime: { api: requestAniList, threshold: 0.6 },
    music: { api: requestSpotify, threshold: 0.85 },
};

const switchQuestion = function switchQuestion(chan) {
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const newQuestion = obj.nbQuestionsMax - obj.remainingQuestions;

    if (obj.remainingQuestions >= 1) { // if game is not finished
        if (obj.nbQuestionsMax !== obj.remainingQuestions) {
            chan.send('*Switching question …*');

            // reset question related objects
            obj.question[newQuestion] = [];
            obj.category = {
                name: obj.category.name,
                randomize: obj.category.randomize,
            };
            obj.images[newQuestion] = [];
            obj.answers[newQuestion] = [];

            // reset players' hints
            Object.keys(obj.players).forEach((playerID) => {
                obj.players[playerID] = {
                    initials_hint: false,
                    image_hint: false,
                    score: 0 || obj.players[playerID].score,
                };
            });
        }

        obj.remainingQuestions -= 1;

        // choose the category randomly if it hasn't been defined by the user when starting the game
        if (obj.category.randomize) {
            const probability = Math.random();
            Object.keys(categoryAPIs).forEach((category) => {
                if (probability > categoryAPIs[category].threshold) {
                    obj.category.name[newQuestion] = category;
                }
            });
        } else {
            obj.category.name[newQuestion] = obj.category.name[0];
        }

        console.log('\n\t===== NEW QUESTION =====\n');
        console.log(` ~ Current category : ${obj.category.name[newQuestion]}\n`);

        fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj)); // save game object

        // call the appropriate API
        const callApi = function callApi() {
            return categoryAPIs[obj.category.name[newQuestion]]
                .api(chan)
                .catch(() => callApi());
        };

        callApi().then((answers) => {
            console.log(`\n > Current answer(s) :\n   - ${answers.slice(0, 10).join('\n   - ')}`);
            if (answers.length > 10) { console.log(`    … and ${answers.length - 10} others`); }
            displayQuestion(chan);
        }).catch((error) => {
            console.error(`${error}`);
        });
    } else { // here, game is over
        displayAllScores(chan, true);
        fs.unlinkSync(`${__dirname}/games/${chanID}.json`);

        console.log('\n< The game has ended >');
    }
};

module.exports = switchQuestion;
