const fs = require('fs');

const addPoints = function addPoints(chan, player) {
    const playerID = player.id.toString();
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));

    // if the player has not yet answered a question, put default values
    if (!obj.players[playerID]) {
        obj.players[playerID] = {
            initials_hint: false,
            image_hint: false,
            score: 0,
        };
    }

    const playerObj = obj.players[playerID];
    const scoreAdded = 1 / (2 ** (playerObj.initials_hint + playerObj.image_hint));

    // add score to player
    playerObj.score += scoreAdded;

    // save modifications to the json
    fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj));

    return scoreAdded;
};

module.exports = addPoints;
