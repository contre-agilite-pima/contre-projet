const fs = require('fs');
const levenshtein = require('js-levenshtein');

const checkAnswer = function checkAnswer(chan, answerProvided, percentageError = 0.2) {
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;
    const currentAnswers = obj.answers[currentQuestion];

    // removal of annoying accent in user input
    const userAnswer = answerProvided.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    for (let i = 0; i < currentAnswers.length; i++) {
        // same for the answers to the question
        const correctAnswer = currentAnswers[i].normalize('NFD').replace(/[\u0300-\u036f]/g, '');

        // calculate levenshtein distance and an error percentage
        // depending on the correct answer length
        const levenshteinDist = levenshtein(correctAnswer.toLowerCase(), userAnswer.toLowerCase());
        const error = levenshteinDist / correctAnswer.length;

        // a certain margin of error is allowed.
        if (error <= percentageError) {
            return true;
        }
    }
    return false;
};

module.exports = checkAnswer;
