const fs = require('fs');

const displayHintImages = function displayHintImages(chan, player) {
    const playerID = player.id.toString();
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;
    const moviesCount = obj.question[currentQuestion].length;

    // if the player has not yet answered a question, put default values
    if (!obj.players[playerID]) {
        obj.players[playerID] = {
            initials_hint: false,
            image_hint: false,
            score: 0,
        };
    }

    if (!obj.players[playerID].image_hint) { // the user didn't yet ask for an image hint
        // mark the player as having asked for an image hint
        obj.players[playerID].image_hint = true;
        fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj));

        // tell the user to check for private messages
        player.send(`Here's an image hint for the current question on **${chan.guild.name}** (<#${chan.id}>) :`);
        chan.send(`<@${playerID}>, check your private messages for an image hint !`);

        // retrieve the question's images and display them in embeds in a private message
        for (let i = 0; i < moviesCount; i++) {
            const imageAddress = obj.images[currentQuestion][i];
            const embed = {
                color: 0xe2772f,
                image: { url: (imageAddress || '') },
                author: { name: obj.question[currentQuestion][i] },
                description: (imageAddress !== null) ? '' : `No image was found for this ${obj.category.name[currentQuestion]}`,
            };

            player.send({ embed });
        }
    } else { // the user already asked for an image hint : warn him
        chan.send(`<@${playerID}>, an image hint has already been sent in your private messages.`);
    }
};

module.exports = displayHintImages;
