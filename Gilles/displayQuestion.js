const fs = require('fs');

const fileAlreadyExists = require(`${__dirname}/fileAlreadyExists`);

const categoryQuestions = {
    movie: 'Which actor played in ',
    anime: 'Which voice actor played in ',
    music: 'Which artist contributed in ',
};

const displayQuestion = function displayQuestion(chan) {
    const chanID = chan.id.toString();

    if (fileAlreadyExists(`${__dirname}/games/${chanID}.json`)) {
        const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
        const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;

        // build question string
        const formattedQ = obj.question[currentQuestion].map(item => `**« ${item} »**`);
        let mess = `[${obj.nbQuestionsMax - obj.remainingQuestions}/${obj.nbQuestionsMax}] `;
        mess += (obj.category.name[currentQuestion] in categoryQuestions) ? categoryQuestions[obj.category.name[currentQuestion]] : 'Who did ';
        mess = mess.concat(formattedQ.slice(0, -1).join(', '), (formattedQ.length > 1) ? ' and ' : '', formattedQ.slice(-1), ' ?');

        chan.send(mess); // write question in channel
    } else {
        console.log('Error reading question.json file in displayQuestion.js');
    }
};

module.exports = displayQuestion;
