const fs = require('fs');

const categoryQuestions = {
    movie: 'Which actor played in ',
    anime: 'Which voice actor played in ',
    music: 'Which artist contributed in ',
};

const displayHint = function displayHint(chan, player) {
    const playerID = player.id.toString();
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;

    // if the player has not yet answered a question, put default values
    if (!obj.players[playerID]) {
        obj.players[playerID] = {
            initials_hint: false,
            image_hint: false,
            score: 0,
        };
    }

    if (!obj.players[playerID].initials_hint) { // the user didn't yet ask for an hint
        // mark the player as having asked for an hint
        obj.players[playerID].initials_hint = true;
        fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj));

        // embed to be displayed to show hint to the player
        const embed = {
            color: 0xe2772f,
            fields: [],
        };

        // build question string
        const formattedQ = obj.question[currentQuestion].map(item => `« ${item} »`);
        let title = (obj.category.name[currentQuestion] in categoryQuestions) ? categoryQuestions[obj.category.name[currentQuestion]] : 'Who did ';
        title = title.concat(formattedQ.slice(0, -1).join(', '), (formattedQ.length > 1) ? ' and ' : '', formattedQ.slice(-1), ' ?');

        // retrieve hint string based on answers' initials
        const initials = obj.answers[currentQuestion].map(answer => `**${answer.split(/-| /gi).map(x => x[0].toUpperCase()).join('')}**`);
        let mess = `Initials of the possible answer${(initials.length > 1) ? 's' : ''} : `;
        mess = mess.concat(initials.slice(0, -1).join(', '), (initials.length > 1) ? ' or ' : '', initials.slice(-1).join(' or '));

        // push new field
        embed.fields.push({
            name: title,
            value: mess,
        });

        // finally, send message to the player
        chan.send(`<@${playerID}>, check your private messages for an hint !`);
        player.send(`Here's an hint for the current question on **${chan.guild.name}** (<#${chan.id}>) :`, { embed });
    } else { // the user already asked for an hint : warn him
        chan.send(`<@${playerID}>, an hint has already been sent in your private messages.`);
    }
};

module.exports = displayHint;
