const fs = require('fs');

const gameAlreadyExists = require(`${__dirname}/gameAlreadyExists.js`);

const createGame = function createGame(chan, message) {
    const chanID = chan.id.toString();
    const defaultQuestionsMax = 2;
    const categories = ['movie', 'anime', 'music'];
    const symbols = ['>', '<', '='];

    // if a file is already registered for this chan, don't do anything
    if (gameAlreadyExists(chan)) {
        return false;
    }

    // the object to write in the json file
    const obj = {
        idOP: message.author.id.toString(),
        nbQuestionsMax: defaultQuestionsMax,
        remainingQuestions: defaultQuestionsMax,
        yearlimit: {
            symbol: '>',
            year: 0,
        },
        players: {},
        question: [[]],
        category: {
            name: [],
            randomize: true,
        },
        images: [[]],
        answers: [[]],
    };

    // check for game parameters in the !gilles start command
    const split = message.content.toLowerCase().split(' ').map(x => x.toLowerCase());
    if (split.length >= 3) {
        for (let i = 2; i < split.length; i++) {
            // first case : this is number, so it defines the maximum number of questions
            if (/^[0-9]+$/.test(split[i])) {
                obj.nbQuestionsMax = (split[i] >= 1) ? split[i] : obj.nbQuestionsMax;
                obj.remainingQuestions = obj.nbQuestionsMax;
            // second case : this is a year with '<', '>' or '=' before
            } else if (symbols.includes(split[i].charAt(0))) {
                obj.yearlimit.symbol = split[i].charAt(0);
                obj.yearlimit.year = parseInt(split[i].substring(1), 10);
            // third case : this is a string, so it defines the category of the questions
            } else {
                const isValidCategory = categories.includes(split[i]);
                obj.category.name[0] = (isValidCategory) ? split[i] : obj.category.name[0];
                obj.category.randomize = (isValidCategory) ? false : obj.category.randomize;
            }
        }
    }

    // Create the games' folder if needed
    try {
        fs.statSync(`${__dirname}/games/`);
    } catch (err) {
        fs.mkdirSync(`${__dirname}/games/`);
    }

    // write the object to the json file for this chan
    fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj));

    return {
        nbQuestions: obj.nbQuestionsMax,
        categoryName: obj.category.name[0],
        year: obj.yearlimit,
    };
};

module.exports = createGame;
