// index.js

// Classes
const fs = require('fs');

// Bot's classes
const Discord = require('discord.js');

const token = require(`${__dirname}/token.js`);

// Functions
const displayQuestion = require(`${__dirname}/displayQuestion.js`);
const displayScore = require(`${__dirname}/displayScore.js`);
const displayAllScores = require(`${__dirname}/displayAllScores.js`);
const displayHistory = require(`${__dirname}/displayHistory.js`);

const displayHint = require(`${__dirname}/displayHint.js`);
const displayHintImages = require(`${__dirname}/displayHintImages.js`);

const createGame = require(`${__dirname}/createGame.js`);
const addPoints = require(`${__dirname}/addPoints.js`);
const switchQuestion = require(`${__dirname}/switchQuestion.js`);
const readAnswer = require(`${__dirname}/readAnswer.js`);
const checkAnswer = require(`${__dirname}/checkAnswer.js`);

const readGameOP = require(`${__dirname}/readGameOP.js`);
const gameAlreadyExists = require(`${__dirname}/gameAlreadyExists.js`);

const bot = new Discord.Client();


// Connecting the bot
bot.login(token)
    .then(() => console.log('Logged in'))
    .catch(console.error);


// When the bot is ready, we update its informations
bot.on('ready', () => {
    // Change bot's playing game
    bot.user.setGame('!help')
        .then(() => console.log('Game changed'))
        .catch(console.error);
});

// Message to be displayed whenever a user asks for help
const embed = {
    color: 0x6fe52b,
    fields: [
        {
            name: 'Starting a game',
            value: '- **`!gilles start <nb_questions>`** : start a new game with the entered number of questions *(defaults to 2)*\n\n' +
                   '- **`!gilles start <your_category=movie,anime,music>`** : start a new game with the selected category for questions *(defaults to a random category for each question)*\n\n' +
                   '- **`!gilles start <{>,<,=}year>`** : start a new game with a year constraint for movies\n\n' +
                   '- **`!gilles start [nb_questions] [your_category=movie,anime,music] [{>,<,=}year]`** : combine all the options you want among the 3 described above',
        },
        {
            name: 'Universal commands',
            value: '- **`!r <your_answer>`** : answer the current question\n\n' +
                   '- **`!score (<all>)`** : display your score *(or the score of every player)*\n\n' +
                   '- **`!score @player1 @player2 …`** : display the score of the given players\n\n' +
                   '- **`!hint`** : ask for an hint about the actor\'s name *(divides points won for the current round by two - cumulative)*\n\n' +
                   '- **`!images`** : see the images *(divides points won for the current round by two - cumulative)*\n\n' +
                   '- **`!repeat`** : show the question once again\n\n' +
                   '- **`!history`** : see the current game\'s history',
        },
        {
            name: 'Owner-exclusive commands',
            value: '- **`!skip`** : skip the current question and move on to the next one\n\n' +
                   '- **`!stop`** : stop the current game',
        },
    ],
};

// Action to run as soon as a new message is posted
bot.on('message', async (message) => {
    const chan = message.channel;
    const authorID = message.author.id.toString();
    const gameRunning = gameAlreadyExists(chan);
    const isOP = (authorID === readGameOP(chan, gameRunning));

    // Case : the OP wants to start the game
    if (message.content.toLowerCase().startsWith('!gilles start')) {
        const gameInformations = createGame(chan, message); // create a game

        // display message accordingly
        if (gameInformations) {
            let startMessage = `Game created with **${gameInformations.nbQuestions} questions** !`;
            startMessage += (gameInformations.categoryName) ? ` The category is : **${gameInformations.categoryName}**.` : '';
            startMessage += '\nIf you want to know what you can do, type `!gilles help`.';

            console.log('\n< The game has started >');
            chan.send(startMessage);
            switchQuestion(chan); // switch and display the question
        } else {
            chan.send('A game is already running in this channel ! A new game was not created.');
        }
    // Case : the player answers the question
    } else if (message.content.toLowerCase().startsWith('!r ') && gameRunning) {
        const userAnswer = readAnswer(message, gameRunning);
        const isAnswerCorrect = checkAnswer(chan, userAnswer);

        if (isAnswerCorrect) { // if the provided answer is correct
            const scoreBoost = addPoints(chan, message.author); // add points to player
            console.log('\n< The question has been answered >');
            chan.send(`<@${authorID}> : correct answer ! +${scoreBoost} point${(scoreBoost === 1) ? '' : 's'}.`);
            switchQuestion(chan); // switch and display the question
        } else { // if the answer is wrong
            chan.send(`<@${authorID}> : wrong answer …`);
        }
    // Case : the OP wants to skip the question
    } else if (message.content.toLowerCase().startsWith('!skip') && gameRunning && isOP) {
        console.log('\n< The question has been skipped >');
        switchQuestion(chan); // switch and display the question
    // Case : the player wants to see the help
    } else if (message.content.toLowerCase().startsWith('!help')) {
        chan.send(`<@${authorID}>, here's some help !`, { embed });
    // Case : the player wants to see everyone's scores
    } else if (message.content.toLowerCase().startsWith('!score all') && gameRunning) {
        displayAllScores(chan);
    // Case : the player wants to see their score or the score of one or multiple players
    } else if (message.content.toLowerCase().startsWith('!score') && gameRunning) {
        const mentions = message.mentions.members.array();
        if (mentions.length) { // if there are mentions
            mentions.forEach((mention) => { displayScore(chan, message, mention.user); });
        } else { // if no one is mentioned, shows the author's score
            displayScore(chan, message, message.author);
        }
    // Case : the player wants to get an hint
    } else if (message.content.toLowerCase().startsWith('!hint') && gameRunning) {
        displayHint(chan, message.author);
    // Case : the player wants to get an image hint
    } else if (message.content.toLowerCase().startsWith('!images') && gameRunning) {
        displayHintImages(chan, message.author);
    // Case : the player wants to see the question again
    } else if (message.content.toLowerCase().startsWith('!repeat') && gameRunning) {
        displayQuestion(chan);
    // Case : the OP wants to stop the game
    } else if (message.content.toLowerCase().startsWith('!stop') && gameRunning && isOP) {
        console.log('\n< The game has been stopped >');
        chan.send('The game will now stop as asked by its creator.');
        displayAllScores(chan, true);
        fs.unlinkSync(`${__dirname}/games/${chan.id}.json`);
    // Case : the player wants to see the history
    } else if (message.content.toLowerCase().startsWith('!history') && gameRunning) {
        displayHistory(chan, message.author);
    }
});
// end bot.on
