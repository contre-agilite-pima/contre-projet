const fs = require('fs');

const displayAllScores = function displayAllScores(chan, endgame = false) {
    const chanID = chan.id.toString();
    const myObject = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const players = Object.entries(myObject.players);
    const rankEmojis = ['🥇', '🥈', '🥉'];

    const mess = (endgame) ? 'Game ended ! Thank you for playing.' : '';

    const embed = {
        color: 0x2bade5,
        description: '',
        author: {
            name: (endgame) ? 'Final scores' : 'Current standings',
        },
    };

    if (players.length < 1 || players === undefined) {
        embed.description += (endgame) ? 'Somehow, nobody scored during this game …' : 'Nobody scored yet !';
    } else {
        let answered = false; // store whether someone answered or not
        players.sort((a, b) => b[1].score - a[1].score); // sort players by score
        players.forEach((couple, index) => {
            if (couple[1].score) {
                answered = true;
                const emoji = (rankEmojis[index]) ? rankEmojis[index] : '🎖';
                embed.description += `${emoji} <@${couple[0]}> : ${couple[1].score} point${(couple[1].score > 1) ? 's' : ''}\n\n`;
            }
        });
        embed.description = (answered) ? embed.description : 'Somehow, nobody scored during this game …';
    }

    chan.send(mess, { embed });
};

module.exports = displayAllScores;
