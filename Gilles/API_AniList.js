const fs = require('fs');
const fetch = require('node-fetch');

const query = `
query ($perPage: Int, $page: Int) {
  Page(perPage: $perPage, page: $page) {
    media(sort: SCORE_DESC) {
      id
      title {
        romaji
      }
      coverImage {
        large
      }
      characters {
        edges {
          node {
            id
          }
          voiceActors(language: JAPANESE) {
            id
            name {
              first
              last
            }
          }
        }
      }
    }
  }
}
`;

const requestAniList = function requestAniList(chan, numberOfPicks = 2) {
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));
    const currentQuestion = obj.nbQuestionsMax - obj.remainingQuestions - 1;

    // API route and options
    const url = 'https://graphql.anilist.co';
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
        body: JSON.stringify({
            query,
            variables: {
                perPage: 10,
                page: 1 + Math.floor(Math.random() * 250),
            },
        }),
    };

    return fetch(url, options)
        .then(response => response.json().then(json => (response.ok ? json : Promise.reject(json))))
        .then((data) => {
            const { media } = data.data.Page;
            const animes = media.sort(() => 0.5 - Math.random()).slice(0, numberOfPicks);

            // TODO : limiter la date ici ? ==> pas de date à priori dans le json data...

            // intersect voice actors - if no possible answer yet, fill with current cast
            for (let i = 0; i < numberOfPicks; i++) {
                const anime = animes[i];

                // for each character, concatenate their voice actors
                obj.answers[currentQuestion] =
                    anime.characters.edges.map(character => character.voiceActors.map(voice => [voice.name.first || '', voice.name.last || ''].join(' ')))
                        .reduce((acc, curr) => acc.concat(curr))
                        // then, look in current answers and intersect values
                        .filter(n => (obj.answers[currentQuestion].length === 0) || (obj.answers[currentQuestion].includes(n)));

                // if those animes have no common voice actor, then pick new animes
                if (obj.answers[currentQuestion].length === 0) {
                    console.error('Problem : could not pick an adequate question ...');
                    console.log('Picking new data.\n');
                    return Promise.reject(new Error(`No common voice actor for animes#${animes.map(x => x.id).join(', ')}`));
                }
            }

            // add the appropriate informations from animes to the game's object
            animes.forEach((anime, index) => {
                obj.question[currentQuestion][index] = anime.title.romaji;
                obj.images[currentQuestion][index] = anime.coverImage.large;
                console.log(` > Question element #${index + 1} : ${obj.question[currentQuestion][index]} (${anime.id})`);
            });

            fs.writeFileSync(`${__dirname}/games/${chanID}.json`, JSON.stringify(obj)); // save the game's object
            return obj.answers[currentQuestion];
        });
};

module.exports = requestAniList;
