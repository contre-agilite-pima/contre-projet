const fs = require('fs');

const categoryQuestions = {
    movie: 'Which actor played in ',
    anime: 'Which voice actor played in ',
    music: 'Which artist contributed in ',
};

const displayHistory = function displayHistory(chan, player) {
    const playerID = player.id.toString();
    const chanID = chan.id.toString();
    const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`));

    if (obj.question.length > 1) {
        // embed to be displayed whenever a player asks for history
        const embed = {
            color: 0x9542f4,
            fields: [],
        };

        // for each question, add a field to the embed
        obj.question.slice(0, -1).forEach((question, index) => {
            // build question string message for that embed field
            const formattedQ = question.map(item => `**« ${item} »**`);
            let mess = (obj.category.name[index] in categoryQuestions) ? categoryQuestions[obj.category.name[index]] : 'Who did ';
            mess = mess.concat(formattedQ.slice(0, -1).join(', '), (formattedQ.length > 1) ? ' and ' : '', formattedQ.slice(-1), ' ?');

            // build answer string for that embed field
            const formattedA = obj.answers[index].map(answer => `**${answer}**`);
            mess = mess.concat('\n   🡒 ', formattedA.slice(0, -1).join(', '), (formattedA.length > 1) ? ' or ' : '', formattedA.slice(-1).join(' or '));

            // push new field
            embed.fields.push({
                name: `Question ${index + 1}`,
                value: mess,
                inline: true,
            });
        });

        // finally, send message to the player
        chan.send(`<@${playerID}>, check your private messages for game history.`);
        player.send(`Here's the game history on **${chan.guild.name}** (<#${chan.id}>) :`, { embed });
    } else {
        chan.send(`<@${playerID}>, there isn't yet a game history.`);
    }
};

module.exports = displayHistory;
