const fs = require('fs');

const readGameOP = function readGameOP(chan, gameRunning) {
    const chanID = chan.id.toString();

    // Retrieve the ID of the game OP if game is running
    if (gameRunning) {
        const obj = JSON.parse(fs.readFileSync(`${__dirname}/games/${chanID}.json`, 'utf8'));
        return obj.idOP;
    }

    return undefined; // Return undefined as OP's ID if no game is running
};

module.exports = readGameOP;
