const fs = require('fs');

const fileAlreadyExists = function fileAlreadyExists(path) {
    return fs.existsSync(path);
};

module.exports = fileAlreadyExists;
