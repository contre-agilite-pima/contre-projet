# Groupe n°13

## Informations sur le groupe

### Composition de l'équipe

| Nom                     | Prénom                   | Email                                  |
| -------------           |-------------             |-------------                           |
| COCCHI                  | Kévin                    | kevin.cocchi@gmail.com                 |
| MONSINJON               | Nicolas                  | nicolas.monsinjon@gmail.com            |
| BERNEAUX                | Damien                   | damien.berneaux@ensiie.fr              |
| BARBIER                 | Anthony                  | phoko.ensiie@gmail.com                 |
| GOUEDARD                | Félix                    | felix.gouedard@free.fr                 |
| MARTIN                  | Loïc                     | l.martinaudonneau@gmail.com            |

### Sujet : Qui a joué dans les 2 films ?

#### Un petit jeu pour les cinéphiles

Quelle actrice retrouve t’on dans "Entretien avec un vampire" et "Spiderman" ? Quel acteur a officié dans "Les tontons flingueurs" et "Astérix et Obélix : Mission Cléopatre" ? Quel duo d’acteurs ont travaillés ensemble sur la série "Sherlock" et dans "Le Hobbit" ?

L’objectif est de mettre en place ce jeu, en utilisant une base de film comme [IMDb](http://www.imdb.com/).

Il s'agira de faire en sorte que les joueurs s’amusent et puissent comparer leur score !

#### Difficultés relatives au projet

- Connexion à une API pour récupérer des données
- Gérer les erreurs lors d'une réponse (ex : est ce que MorganE freeman est une réponse valide ?)
- Tout le monde n’a pas vu tous les films : comment faire en sorte qu'ils s'amusent ?
- Comment gérer la difficulté ?
- Comment orchestrer le jeu en lui même ?


### Lien vers le Trello

[Trello](https://trello.com/b/WbMZS4kw/qui-a-jou%C3%A9-dans-les-2-films)

## Partie libre

### Avant de commencer

Avant de commencer :
- `git clone git@gitlab.com:contre-agilite-pima/contre-projet.git` (SSH)
- `https://gitlab.com/contre-agilite-pima/contre-projet.git` (HTTPS)

Dans le dossier du projet, penser à `npm install` une fois cloné (*ou à chaque changement des dépendances*).

### Setup du linter sur Atom

Installer le package [linter-eslint](https://atom.io/packages/linter-eslint). Éventuellement cocher `Fix errors on save` pour corriger les erreurs à chaque sauvegarde des fichiers.

Il n'y a normalement rien d'autre à faire pour faire fonctionner le linter (hormis un `npm install`).

### Autres remarques

Penser à `git pull` assez régulièrement.
